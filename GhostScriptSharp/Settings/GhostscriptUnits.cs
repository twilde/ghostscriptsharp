﻿namespace GhostscriptSharp.Settings
{
   public enum GhostscriptUnits
   {
      Points,
      Pixels
   }
}
