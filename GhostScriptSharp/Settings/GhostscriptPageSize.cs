namespace GhostscriptSharp.Settings
{
   using System.Drawing;

   /// <summary>
   ///    Output document physical dimensions
   /// </summary>
   public class GhostscriptPageSize
   {
      private GhostscriptPageSizes native;
      private Size manual;

      /// <summary>
      ///    Custom document size
      /// </summary>
      public Size Manual
      {
         set
         {
            native = GhostscriptPageSizes.UNDEFINED;
            manual = value;
         }
         get { return manual; }
      }

      /// <summary>
      ///    Standard paper size
      /// </summary>
      public GhostscriptPageSizes Native
      {
         set
         {
            native = value;
            manual = new Size( 0, 0 );
         }
         get { return native; }
      }

      public GhostscriptUnits Units { get; set; }
   }
}
