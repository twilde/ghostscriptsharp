namespace GhostscriptSharp.Settings
{
   /// <summary>
   ///    Which pages to output
   /// </summary>
   public class GhostscriptPages
   {
      private bool allPages = true;
      private int start;
      private int end;

      public static GhostscriptPages All
      {
         get { return new GhostscriptPages { AllPages = true }; }
      }

      /// <summary>
      ///    Output all pages avaialble in document
      /// </summary>
      public bool AllPages
      {
         set
         {
            start = -1;
            end = -1;
            allPages = value;
         }
         get { return allPages; }
      }

      /// <summary>
      ///    Start output at this page (1 for page 1)
      /// </summary>
      public int Start
      {
         set
         {
            allPages = false;
            start = value;
         }
         get { return start; }
      }

      /// <summary>
      ///    Page to stop output at
      /// </summary>
      public int End
      {
         set
         {
            allPages = false;
            end = value;
         }
         get { return end; }
      }
   }
}
