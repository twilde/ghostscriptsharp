﻿namespace GhostscriptSharp.Settings
{
   using System;

   public class SizeAttribute: Attribute
   {
      public string GhostscriptName { get; set; }
   }
}
