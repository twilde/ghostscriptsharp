namespace GhostscriptSharp.Settings
{
   using System.Drawing;

   /// <summary>
   ///    Ghostscript settings
   /// </summary>
   public class GhostscriptSettings
   {
      public GhostscriptSettings()
      {
         Size = new GhostscriptPageSize();
         Page = new GhostscriptPages();
      }

      public GhostscriptDevices Device { get; set; }

      public GhostscriptPages Page { get; set; }

      public Size Resolution { get; set; }

      public GhostscriptPageSize Size { get; set; }

      public GhostscriptUnits Units { get; set; }
   }
}
