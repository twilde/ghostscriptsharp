﻿namespace GhostscriptSharp.Settings
{
   using System.Linq;

   public static class GhostscriptPageSizesExtensions
   {
      public static string GhostscriptName( this GhostscriptPageSizes pageSize )
      {
         var size = (SizeAttribute) pageSize.GetType().GetCustomAttributes( typeof( SizeAttribute ), false ).FirstOrDefault();

         if( size == null )
            return pageSize.ToString();

         return size.GhostscriptName;
      }
   }
}
