namespace GhostscriptSharp
{
   using System;
   using System.Collections;
   using System.Drawing;
   using API;
   using Settings;

   /// <summary>
   ///    Wraps the Ghostscript API with a C# interface
   /// </summary>
   public class Ghostscript
   {
      private static readonly string[] args =
      {
         // Keep gs from writing information to standard output
         "-q",
         "-dQUIET",
         "-dPARANOIDSAFER", // Run this command in safe mode
         "-dBATCH", // Keep gs from going into interactive mode
         "-dNOPAUSE", // Do not prompt and pause for each page
         "-dNOPROMPT", // Disable prompts for user interaction           
         "-dMaxBitmap=500000000", // Set high for better performance
         "-dNumRenderingThreads=" + Environment.ProcessorCount, // Multi-core, come-on!

         // Configure the output anti-aliasing, resolution, etc
         "-dAlignToPixels=0",
         "-dGridFitTT=0",
         "-dTextAlphaBits=4",
         "-dGraphicsAlphaBits=4"
      };

      /// <summary>
      ///    Generates a thumbnail jpg for the pdf at the input path and saves it
      ///    at the output path
      /// </summary>
      public static void GeneratePageThumb( string inputPath, string outputPath, int page, int width = 0, int height = 0, int dpix = 72, int dpiy = 72,
                                            GhostscriptUnits units = GhostscriptUnits.Points )
      {
         GeneratePageThumbs( inputPath, outputPath, page, page, width, height, dpix, dpiy, units );
      }

      /// <summary>
      ///    Generates a collection of thumbnail jpgs for the pdf at the input path
      ///    starting with firstPage and ending with lastPage.
      ///    Put "%d" somewhere in the output path to have each of the pages numbered
      /// </summary>
      public static void GeneratePageThumbs( string inputPath, string outputPath, int firstPage, int lastPage, int width = 0, int height = 0, int dpix = 72, int dpiy = 72,
                                             GhostscriptUnits units = GhostscriptUnits.Points )
      {
         if( Environment.Is64BitProcess )
            GhostScript64.CallAPI( GetArgs( inputPath, outputPath, firstPage, lastPage, dpix, dpiy, width, height, units ) );
         else
            GhostScript32.CallAPI( GetArgs( inputPath, outputPath, firstPage, lastPage, dpix, dpiy, width, height, units ) );
      }

      /// <summary>
      ///    Rasterises a PDF into selected format
      /// </summary>
      /// <param name="inputPath">PDF file to convert</param>
      /// <param name="outputPath">Destination file</param>
      /// <param name="settings">Conversion settings</param>
      public static void GenerateOutput( string inputPath, string outputPath, GhostscriptSettings settings )
      {
         if( Environment.Is64BitProcess )
            GhostScript64.CallAPI( GetArgs( inputPath, outputPath, settings ) );
         else
            GhostScript32.CallAPI( GetArgs( inputPath, outputPath, settings ) );
      }

      /// <summary>
      ///    Returns an array of arguments to be sent to the Ghostscript API
      /// </summary>
      /// <param name="inputPath">Path to the source file</param>
      /// <param name="outputPath">Path to the output file</param>
      /// <param name="firstPage">The page of the file to start on</param>
      /// <param name="lastPage">The page of the file to end on</param>
      /// <param name="dpix"></param>
      /// <param name="dpiy"></param>
      /// <param name="width"></param>
      /// <param name="height"></param>
      private static string[] GetArgs( string inputPath,
                                       string outputPath,
                                       int firstPage,
                                       int lastPage,
                                       int dpix,
                                       int dpiy,
                                       int width,
                                       int height,
                                       GhostscriptUnits units )
      {
         // To maintain backwards compatibility, this method uses previous hardcoded values.

         var settings = new GhostscriptSettings
                        {
                           Device = GhostscriptDevices.jpeg,
                           Page = { Start = firstPage, End = lastPage },
                           Resolution = new Size( dpix, dpiy )
                        };

         var pageSize = new GhostscriptPageSize();
         if( width == 0 && height == 0 )
            pageSize.Native = GhostscriptPageSizes.a7;
         else
         {
            pageSize.Manual = new Size( width, height );
            pageSize.Units = units;
         }
         settings.Size = pageSize;

         return GetArgs( inputPath, outputPath, settings );
      }

      /// <summary>
      ///    Returns an array of arguments to be sent to the Ghostscript API
      /// </summary>
      /// <param name="inputPath">Path to the source file</param>
      /// <param name="outputPath">Path to the output file</param>
      /// <param name="settings">API parameters</param>
      /// <returns>API arguments</returns>
      private static string[] GetArgs( string inputPath,
                                       string outputPath,
                                       GhostscriptSettings settings )
      {
         var arguments = new ArrayList( args );

         if( settings.Device == GhostscriptDevices.UNDEFINED )
            throw new ArgumentException( "An output device must be defined for Ghostscript", "GhostscriptSettings.Device" );

         if( settings.Page.AllPages == false && ( settings.Page.Start <= 0 && settings.Page.End < settings.Page.Start ) )
            throw new ArgumentException( "Pages to be printed must be defined.", "GhostscriptSettings.Pages" );

         if( settings.Resolution.IsEmpty )
            throw new ArgumentException( "An output resolution must be defined", "GhostscriptSettings.Resolution" );

         if( settings.Size.Native == GhostscriptPageSizes.UNDEFINED && settings.Size.Manual.IsEmpty )
            throw new ArgumentException( "Page size must be defined", "GhostscriptSettings.Size" );

         // Output device
         arguments.Add( String.Format( "-sDEVICE={0}", settings.Device ) );

         // Pages to output
         if( settings.Page.AllPages )
            arguments.Add( "-dFirstPage=1" );
         else
         {
            arguments.Add( String.Format( "-dFirstPage={0}", settings.Page.Start ) );
            if( settings.Page.End >= settings.Page.Start )
               arguments.Add( String.Format( "-dLastPage={0}", settings.Page.End ) );
         }

         // Page size
         if( settings.Size.Native == GhostscriptPageSizes.UNDEFINED )
         {
            if( settings.Size.Units == GhostscriptUnits.Points )
            {
               arguments.Add( String.Format( "-dDEVICEWIDTHPOINTS={0}", settings.Size.Manual.Width ) );
               arguments.Add( String.Format( "-dDEVICEHEIGHTPOINTS={0}", settings.Size.Manual.Height ) );
            }
            else
               arguments.Add( string.Format( "-g{0}x{1}", settings.Size.Manual.Width, settings.Size.Manual.Height ) );

            arguments.Add( "-dFIXEDMEDIA" );
            arguments.Add( "-dPDFFitPage" );
         }
         else
            arguments.Add( String.Format( "-sPAPERSIZE={0}", settings.Size.Native.GhostscriptName() ) );

         // Page resolution
         arguments.Add( String.Format( "-dDEVICEXRESOLUTION={0}", settings.Resolution.Width ) );
         arguments.Add( String.Format( "-dDEVICEYRESOLUTION={0}", settings.Resolution.Height ) );

         // Files
         arguments.Add( String.Format( "-sOutputFile={0}", outputPath ) );
         arguments.Add( inputPath );

         return (string[]) arguments.ToArray( typeof( string ) );
      }
   }
}
