param($installPath, $toolsPath, $package, $project)

$projectItems = $project.ProjectItems

$gs32dll = $projectItems.Item("gsdll32.dll")
$gs32dll.Properties.Item("BuildAction").Value = 0 # BuildAction = None
$gs32dll.Properties.Item("CopyToOutputDirectory").Value = 1 # CopyToOutputDirectory = Copy always


$gs64dll = $projectItems.Item("gsdll64.dll")
$gs64dll.Properties.Item("BuildAction").Value = 0 # BuildAction = None
$gs64dll.Properties.Item("CopyToOutputDirectory").Value = 1 # CopyToOutputDirectory = Copy always