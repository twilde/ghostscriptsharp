namespace GhostscriptSharpTests
{
   using System.Drawing;
   using System.IO;
   using ApprovalTests;
   using ApprovalTests.Reporters;
   using GhostscriptSharp;
   using GhostscriptSharp.Settings;
   using NUnit.Framework;

   [ TestFixture ]
   public class GhostscriptSharpImageContentTests
   {
      private FileInfo destination;
      private const string TEST_FILE_LOCATION = "test.pdf";

      [ SetUp ]
      public void Set_up_test_context()
      {
         destination = new FileInfo( Path.Combine( Path.GetTempPath(), "GhostscriptSharpImageContentTests.ConvertToPNG.png" ) );
      }

      [ Test, RequiresSTA, UseReporter( typeof( FileLauncherReporter ) ) ]
      public void ConvertToPNG()
      {
         Ghostscript.GenerateOutput( TEST_FILE_LOCATION,
                                     destination.FullName,
                                     new GhostscriptSettings
                                     {
                                        Device = GhostscriptDevices.png16m,
                                        Resolution = new Size( 400, 400 ),
                                        Page = new GhostscriptPages { Start = 1, End = 1 },
                                        Size = new GhostscriptPageSize { Native = GhostscriptPageSizes.a4 },
                                     } );

         Approvals.VerifyFile( destination.FullName );
      }
   }
}
