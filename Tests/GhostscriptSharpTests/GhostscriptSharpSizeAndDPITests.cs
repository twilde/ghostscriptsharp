﻿namespace GhostscriptSharpTests
{
   using System;
   using System.Drawing;
   using System.IO;
   using GhostscriptSharp;
   using GhostscriptSharp.Settings;
   using NUnit.Framework;

   [ TestFixture ]
   public class GhostscriptSharpSizeAndDPITests
   {
      private const string TEST_FILE_LOCATION = "test.pdf";
      private const int MULTIPLE_FILE_PAGE_COUNT = 10;
      private const float POSTSCRIPT_POINTS_PER_INCH = 72f;

      private string singleFileLocation;
      private string multipleFileLocation;
      private string multipleFileLocationTemplate;

      [ TestFixtureSetUp ]
      public void SetUp()
      {
         var temporaryPath = Path.GetTempPath();
         singleFileLocation = Path.Combine( temporaryPath, "output.jpg" );
         multipleFileLocationTemplate = Path.Combine( temporaryPath, "output{0}.jpg" );
         multipleFileLocation = string.Format( multipleFileLocationTemplate, "%d" );
      }

      [ Test ]
      public void GenerateSinglePageThumbnail()
      {
         const int width = 100,
                   height = 100;

         Ghostscript.GeneratePageThumb( TEST_FILE_LOCATION, singleFileLocation, 1, width, height );

         File.Exists( singleFileLocation ).Should( Be.True );

         using( var image = Image.FromFile( singleFileLocation ) )
         {
            image.Size.Width.Should( Be.EqualTo( width ) );
            image.Size.Height.Should( Be.EqualTo( height ) );
            image.HorizontalResolution.Should( Be.EqualTo( POSTSCRIPT_POINTS_PER_INCH ) );
            image.VerticalResolution.Should( Be.EqualTo( POSTSCRIPT_POINTS_PER_INCH ) );
         }
      }

      [ Test ]
      public void GenerateMultiplePageThumbnails()
      {
         const int width = 200,
                   height = 300;

         Ghostscript.GeneratePageThumbs( TEST_FILE_LOCATION, multipleFileLocation, 1, MULTIPLE_FILE_PAGE_COUNT, width, height );

         for( var i = 1; i <= MULTIPLE_FILE_PAGE_COUNT; i++ )
         {
            var filename = String.Format( multipleFileLocationTemplate, i );
            File.Exists( filename ).Should( Be.True );

            using( var image = Image.FromFile( filename ) )
            {
               image.Width.Should( Be.EqualTo( width ) );
               image.Height.Should( Be.EqualTo( height ) );
               image.HorizontalResolution.Should( Be.EqualTo( POSTSCRIPT_POINTS_PER_INCH ) );
               image.VerticalResolution.Should( Be.EqualTo( POSTSCRIPT_POINTS_PER_INCH ) );
            }
         }
      }

      [ Test ]
      public void GenerateThumbnailWithAlternativeDPI()
      {
         const int width = 200,
                   height = 300,
                   dpix = 92,
                   dpiy = 92;

         var expectedWidth = Math.Round( width / ( POSTSCRIPT_POINTS_PER_INCH / dpix ) );
         var expectedHeight = Math.Round( height / ( POSTSCRIPT_POINTS_PER_INCH / dpiy ) );

         Ghostscript.GeneratePageThumb( TEST_FILE_LOCATION, singleFileLocation, 1, width, height, dpix, dpiy );

         File.Exists( singleFileLocation ).Should( Be.True );
         using( var image = Image.FromFile( singleFileLocation ) )
         {
            image.Width.Should( Be.EqualTo( expectedWidth ) );
            image.Height.Should( Be.EqualTo( expectedHeight ) );

            image.HorizontalResolution.Should( Be.EqualTo( dpix ) );
            image.VerticalResolution.Should( Be.EqualTo( dpiy ) );
         }
      }

      [ Test ]
      public void GenerateThumbnailWithAlternativeDPIAndSpecificPixelSizing()
      {
         const int width = 240,
                   height = 320,
                   dpix = 92,
                   dpiy = 92;

         Ghostscript.GeneratePageThumb( TEST_FILE_LOCATION, singleFileLocation, 1, width, height, dpix, dpiy, GhostscriptUnits.Pixels );

         File.Exists( singleFileLocation ).Should( Be.True );

         using( var image = Image.FromFile( singleFileLocation ) )
         {
            image.Width.Should( Be.EqualTo( width ) );
            image.Height.Should( Be.EqualTo( height ) );

            image.HorizontalResolution.Should( Be.EqualTo( dpix ) );
            image.VerticalResolution.Should( Be.EqualTo( dpiy ) );
         }
      }

      [ TearDown ]
      public void Cleanup()
      {
         File.Delete( singleFileLocation );
         for( var i = 1; i <= MULTIPLE_FILE_PAGE_COUNT; i++ )
            File.Delete( String.Format( multipleFileLocationTemplate, i ) );
      }
   }
}
